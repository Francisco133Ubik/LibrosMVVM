//
//  CoreDataStack.swift
//  LibrosMVVM
//
//  Created by Francisco on 07/03/22.
//

import CoreData

func createContainer(completion: @escaping (NSPersistentContainer) -> ()) {
    let containter = NSPersistentContainer(name: "CoreData")
    containter.loadPersistentStores { _, error in
        guard error == nil else {
            fatalError("Failed to load store: \(error!)")
        }
        DispatchQueue.main.async {
            completion(containter)
        }
    }
}
